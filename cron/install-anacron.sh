#!/bin/sh

if [[ -d /etc/cron.daily ]]; then
	mkdir -p /opt/debiankaios-de
	cp 'wrapper.sh' '/etc/cron.daily/'
	cp '../autoGatherData.py' '/opt/debiankaios-de'
else
	echo 'Error: Needs /etc/cron.daily set up' 1>&2
fi
