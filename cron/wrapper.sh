#!/bin/bash

# Get path of the website (usually /srv/www or /srv/http)
wwwpath=""
if [[ -d /srv/www ]]; then
	wwwpath='/srv/www'
fi
if [[ -d /srv/http ]]; then
	wwwpath='/srv/http'
fi

ret=0

# Switch to right folder
cd /opt/cron-debiankaios-de
# Backup old data before letting the script write on it
cp './profilestats.json' './profilestats.json~'
# Update old data and make it to new data
if python3 autoGatherData.py > /dev/null 2>/dev/null; then
	ret=0
	# Remove backup because everything was successfull
	rm './profilestats.json~'
else
	echo 'Error: Script crashed' 1>&2
	ret=1
	# Load backup
	rm './profilestats.json'
	mv './profilestats.json~' './profilestats.json'
fi
# Swap old and new data
mv "${wwwpath}/sources/profilestats.json" './profilestats_next.json'
mv './profilestats.json' "${wwwpath}/sources/profilestats.json"
mv './profilestats_next.json' './profilestats.json'

exit "${ret}"
