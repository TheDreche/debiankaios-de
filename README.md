# debiankaios-de

![Screenshot of the final site](images/screenshot-1.png)

This is my imagination of the web site `http://debiankaios.de`. Currently the site owner uses it.

The following just describes the usage on a server.

## Automatically update
This site needs the `profilestats.json` file. It should be placed in the `sources/` directory of the document root.

Every second day, the `./autoGatherData.py` script should be run with the old `profilestats.json` in the same directory as the script. It will update the old data, by using the data which can't be downloaded anymore.

Please note that doing this with a `profilestats.json` which last update was some number of days ago which isn't devidable by 2 will erase the old data.

## Starter profilestats.json
There was some data in the previous version of the website. It is placed under the tools directory now, where also a script to convert the previous data into the new data can be found.

## Automatically do the update
If your server has cron(ie) or anacron and python 3.x installed, this job can be done automatically: You will find all necessary things in the `cron` directory.

**NOTE**: This feature is currently *experimental*!

### Software requirements
* Cron or Cronie or Anacron
* Python 3 (`/bin/python3` should be a valid command, usually a symlink to the most recent version)

### Preparation
You will need to have a starter `profilestats.json`. In case you haven't gotten one, just write an empty JSON inside it:

	{}

The following files should be moved into the `/opt/debiankaios-de` directory:
* autoGatherData.py
* profilestats.json _(I mean your starter one)_

In the `wrapper.sh` file, replace the according part to the document root. It will be used later on.

### Cron(ie)-specifics
In order to use Cron(ie) you have to move the `wrapper.sh` file into the `/opt/debiankaios-de` directory and the `5wwwupdate` file is a starting point for a crontab(5)-entry.

### Anacron-specifics
In order to use Anacron you have to move the `wrapper.sh` script into the `/etc/cron.daily` directory (it requires that this directory is set up right).

### Automatic installation
There are also two scripts made for installation:
* `install-cron.sh` installs everything for Cron(ie). It requires that the `/etc/cron.d` directory can be used.
* `install-anacron.sh` installs everything for anacron. It requires that the `/etc/cron.daily` directory is set up right (usually the case)

Both scripts have to be able to write into all specified directories. Make sure the scripts have the needed permissions. Generally, the following directories should be writable:
* Your Document root (`/srv/http` or `/srv/www` by default)
* `<Document root>/sources`
* `/opt/debiankaios-de`
* For Anacron: `/etc/cron.daily`
* For Cron(ie): `/etc/cron.d`
