#!/usr/bin/python3

import copy
import traceback
import requests
import json
import datetime

today = datetime.date.today()

USE_CACHE: bool = True
MAX_RETRYS : int = 3

DATE_FORMAT: str = "%d.%m.%Y"
SCRATCHDB_DATE_FORMAT: str = "%Y-%m-%dT%H:%M:%S.000Z"

URL_PROJECT_ID: str = "https://scratch.mit.edu/projects/%s"

def debug_capture(func):
	def inner(*args):
		print("Args: ", *args)
		a = func(*args)
		print(a)
		return a
	return inner

def dbgprt(a):
	print(a)
	return a

def dict_get_path(d: dict, path: list, substitutions: list):
	current = d
	subst_at: int = 0
	for p in path:
		if p is None:
			current = current[substitutions[subst_at]]
			subst_at += 1
		else:
			current = current[p]
	return current

def dict_get_path_default(d: dict, path: list, default, substitutions: list):
	try:
		return dict_get_path(d, path, substitutions)
	except Exception:
		return default

cache: dict = {}
session: requests.Session = None

class max_retrys_reached_error(Exception):
	pass

def fetch(url: str) -> dict:
	global session
	if url not in cache:
		if session is None:
			session = requests.Session()
		result: requests.Response = None
		for i in range(MAX_RETRYS):
			print("Requesting	%s" % url)
			result = session.get(url)
			if(result is not None):
				break
			print("Failed!")
		if(result is None):
			print("Max retrys reached; cancelling...")
			raise max_retrys_reached_error()
		cache[url] = result.json()
	return cache[url]

def get_user_projects(username: str) -> list:
	return fetch("https://scratchdb.lefty.one/v2/project/info/user/%s" % username)["projects"]

def get_project_share_date(id: int) -> datetime.date:
	return datetime.datetime.strptime(fetch("https://scratchdb.lefty.one/v3/project/info/%s" % str(id))["times"]["shared"], SCRATCHDB_DATE_FORMAT).date()

def project_is_on_explore(id: int):
	return len(
			list(
				filter(
					lambda p: dict_get_path(p, ["id"], []) == id,
					fetch("https://api.scratch.mit.edu/explore/projects?language=de&limit=16&offset=0&mode=trending")
				)
			)
		) > 0 or len(
			list(
				filter(
					lambda p: dict_get_path(p, ["id"], []) == id,
					fetch("https://api.scratch.mit.edu/explore/projects?language=en&limit=16&offset=0&mode=trending")
				)
			)
		) > 0

def get_data_from(file_data: dict, place: dict, substitutions: list):
	data: dict = file_data
	if "url" in place:
		file: str = place["url"]
		if file is not None and file != "special://file":
			if file == "special://scratchdb/v3/user/graph":
				data = fetch("https://scratchdb.lefty.one/v3/user/graph/%s/%s?segment=%i&range=%i" % tuple(place["path"][:4]))
				date: datetime.date = datetime.datetime.strptime(place["path"][4], DATE_FORMAT).date() if place["path"][4] is not None else substitutions[0]
				filtered: list = list(filter(lambda dated_data: dated_data["date"] == date.strftime(SCRATCHDB_DATE_FORMAT), data))
				if len(filtered) == 0:
					return None
				else:
					return filtered[0]["value"]
			elif file.startswith("file://"):
				with open(file.removeprefix("file://"), "r") as f:
					data = json.load(f)
			else:
				data = fetch(file)
	return dict_get_path_default(data, place["path"], None, substitutions)

def first_not_none(iterable):
	for i in iterable:
		if i is not None:
			return i
	return None

class output_json:
	def __init__(self, data):
		self.data = data
	
	def save_dict(self):
		return self.data
	
	def get_username(self) -> str:
		if "user" in self.data:
			return self.data["user"]
		return None
	
	def set_username(self, user: str):
		self.data["user"] = user
	
	def get_date(self) -> datetime.date:
		if "date" in self.data:
			return datetime.datetime.strptime(self.data["date"], DATE_FORMAT).date()
		return None
	
	def set_date(self, date: datetime.date):
		self.data["date"] = date.strftime(DATE_FORMAT)
	
	def update_date(self):
		self.set_date(today)
	
	def update(self):
		self.update_date()
		self.update_projects_top()
		self.update_project_stats()
		self.update_user_stats()
		self.update_user_stats_history()
	
	def get_projects_top(self) -> list:
		if "projects_top" in self.data:
			return self.data["projects_top"]
		return []
	
	def set_projects_top(self, projects_top: list):
		self.data["projects_top"] = projects_top
	
	def update_projects_top(self):
		projects_top = self.get_projects_top()
		
		projects: list = sorted(
				get_user_projects(
					self.get_username()
				),
				key=lambda d: dict_get_path_default(d, ["views"], 0, []),
				reverse=True
			)[:5]
		old_projects: dict = {dict_get_path(d, ["url"], []) : d for d in self.get_projects_top()}
		
		new_projects: list = []
		for p in projects:
			p_id: int = dict_get_path(p, ["info", "scratch_id"], [])
			url: str = URL_PROJECT_ID % str(p_id)
			add_project: dict = old_projects[url] if url in old_projects else {}
			add_project.update({
				"name": dict_get_path_default(p, ["info", "title"], add_project["name"] if "name" in add_project else None, []),
				"views": dict_get_path_default(p, ["views"], add_project["views"] if "views" in add_project else None, []),
				"share_date": get_project_share_date(p_id).strftime(DATE_FORMAT),
				"explore": (add_project["explore"] if "explore" in add_project else False) or project_is_on_explore(p_id),
				"url": url
			})
			new_projects.append(add_project)
		
		projects_top = new_projects
		self.set_projects_top(projects_top)
	
	def get_project_stats(self) -> dict:
		if "project_stats" in self.data:
			return self.data["project_stats"]
		return {"total": {}, "average": {}}
	
	def set_project_stats(self, project_stats: dict):
		self.data["project_stats"] = project_stats
	
	def update_project_stats(self):
		self.update_project_stats_total()
		self.update_project_stats_average()
	
	def get_project_stats_total(self) -> dict:
		if "total" in self.get_project_stats():
			return self.get_project_stats()["total"]
		return {}
	
	def set_project_stats_total(self, project_stats_total: dict):
		project_stats: dict = self.get_project_stats()
		project_stats["total"] = project_stats_total
		self.set_project_stats(project_stats)
	
	def update_project_stats_total(self):
		project_stats_total: dict = self.get_project_stats_total()
		project_stats_total["views"] = dict_get_path_default(
			fetch("https://scratchdb.lefty.one/v3/user/info/%s" % self.get_username()),
			["statistics", "views"],
			0,
			[]
		)
		self.set_project_stats_total(project_stats_total)
	
	def get_project_stats_average(self) -> dict:
		if "average" in self.get_project_stats():
			return self.get_project_stats()["average"]
		return {}
	
	def set_project_stats_average(self, project_stats_average: dict):
		project_stats: dict = self.get_project_stats()
		project_stats["average"] = project_stats_average
		self.set_project_stats(project_stats)
	
	def update_project_stats_average(self):
		project_stats_average: dict = self.get_project_stats_average()
		project_stats_total: dict = self.get_project_stats_total()
		project_count: int = len(get_user_projects(self.get_username()))
		for i in ["views"]:
			project_stats_average[i] = project_stats_total[i] / project_count
	
	def get_user_stats(self) -> dict:
		if "user_stats" in self.data:
			return self.data["user_stats"]
		return {"follower": {}, "views": {}, "likes": {}, "faves": {}, "comments": {}}
	
	def set_user_stats(self, user_stats: dict):
		self.data["user_stats"] = user_stats
	
	def update_user_stats(self):
		user_stats = self.get_user_stats()
		for output_key, input_key in [("follower", "followers"), ("views", "views"), ("likes", "loves"), ("faves", "favorites"), ("comments", "comments")]:
			update_dict: dict = user_stats[output_key] if output_key in user_stats else {}
			update_dict.update({
				"count": dict_get_path(
					fetch("https://scratchdb.lefty.one/v3/user/info/%s" % self.get_username()),
					["statistics", None],
					[input_key]
				),
				"rank_world": dict_get_path(
					fetch("https://scratchdb.lefty.one/v3/user/info/%s" % self.get_username()),
					["statistics", "ranks", None],
					[input_key]
				),
				"rank_local": dict_get_path(
					fetch("https://scratchdb.lefty.one/v3/user/info/%s" % self.get_username()),
					["statistics", "ranks", "country", None],
					[input_key]
				)
			})
			user_stats[output_key] = update_dict
		self.set_user_stats(user_stats)
	
	def get_user_stats_history(self) -> dict:
		if "user_stats_history" in self.data:
			return self.data["user_stats_history"]
		return {"labels": [], "datasets": []}
	
	def set_user_stats_history(self, user_stats_history: dict):
		self.data["user_stats_history"] = user_stats_history
	
	def update_user_stats_history(self):
		configuration: dict = copy.deepcopy(dict_get_path_default(self.data, ["conf", "user_stats_history"], {}, []))
		user_stats_history: dict = self.get_user_stats_history()
		
		saved_datasets: list = [i["label"] if "label" in i else None for i in user_stats_history.get("datasets", [])]
		saved_datasets = list(filter(lambda i: i is not None, saved_datasets))
		
		conf_datasets: dict = dict_get_path_default(configuration, ["datasets"], {}, [])
		if None in conf_datasets:
			del conf_datasets[None]
		conf_datasets_other: dict = copy.deepcopy(conf_datasets)
		for conf_dataset in conf_datasets_other.keys():
			if "data" in conf_datasets_other[conf_dataset]:
				del conf_datasets_other[conf_dataset]["data"]
			conf_datasets_other[conf_dataset]["label"] = conf_dataset
		
		translate_datasets: dict = {
				d : dict_get_path_default(conf_datasets, [None, "rename"], d, [d]) for d in saved_datasets
		}
		
		new_dates: list = [today]
		while new_dates[0] > today - datetime.timedelta(days=365):
			new_dates.insert(0, new_dates[0] - datetime.timedelta(days=1))
		
		saved_dates: list = [datetime.datetime.strptime(d, DATE_FORMAT).date() for d in dict_get_path_default(user_stats_history, ["labels"], [], [])]
		
		datedata: dict = {
				saved_dates[i]: {
					a["label"]: a["data"][i] for a in dict_get_path_default(user_stats_history, ["datasets"], [], [])
				} for i in range(len(saved_dates))
		}
		
		new_datedata: dict = {
				date: {
					translate_datasets[saved_dataset]: dict_get_path_default(
						datedata,
						[None, None],
						None,
						[date, saved_dataset]
					) for saved_dataset in translate_datasets.keys()
				} for date in new_dates
		}
		
		for new_dataset in translate_datasets.values():
			for translate_data in dict_get_path_default(conf_datasets, [None, "data"], [], [new_dataset]):
				if "path" in translate_data:
					if type(translate_data["path"]) == list:
						# Valid
						if translate_data["path"].count(None) > 0:
							for date in new_dates:
								maybe_new_data: int = get_data_from(self.data, translate_data, [date] * translate_data["path"].count(None))
								if maybe_new_data is not None:
									new_datedata[date][new_dataset] = maybe_new_data
						else:
							maybe_new_data: int = get_data_from(self.data, translate_data, [])
							if maybe_new_data is not None:
								new_datedata[today][new_dataset] = maybe_new_data
		
		user_stats_history["labels"] = [a.strftime(DATE_FORMAT) for a in new_dates]
		user_stats_history["datasets"] = [
			{
				"label": new_dataset,
				"data": [
					new_datedata[date][new_dataset] for date in new_dates
				]
			} for new_dataset in set(translate_datasets.values())
		]
		
		for new_dataset_nr in range(len(user_stats_history["datasets"])):
			if dict_get_path_default(user_stats_history["datasets"][new_dataset_nr], ["label"], None, []) in conf_datasets_other:
				# None can't be in conf_datasets_other
				user_stats_history["datasets"][new_dataset_nr].update(conf_datasets_other[user_stats_history["datasets"][new_dataset_nr]["label"]])
		
		user_stats_history["datasets"].sort(key=lambda i: first_not_none(reversed(i["data"])), reverse=True)
		self.set_user_stats_history(user_stats_history)

def read_cache():
	global cache
	try:
		with open("cache.json", "r") as f:
			cache = json.load(f)
		print("Loaded cache.")
	except FileNotFoundError:
		pass

def save_cache():
	with open("cache.json", "w") as f:
		json.dump(cache, f)

def read_data() -> output_json:
	try:
		with open("profilestats.json", "r") as f:
			return output_json(json.load(f))
	except FileNotFoundError:
		return output_json({"user": "debiankaios"})

def save_data(o: output_json):
	with open("profilestats.json", "w") as f:
		json.dump(o.save_dict(), f, indent="\t", sort_keys=True)

def main():
	try:
		if USE_CACHE:
			read_cache()
		try:
			data = read_data()
			try:
				data.update()
			except Exception as e:
				print("Updating the data failed")
				traceback.print_exc()
				print(e)
			else:
				save_data(data)
		except Exception as e:
			print("Loading or saving actual data failed!")
			traceback.print_exc()
			print(e)
		if USE_CACHE:
			save_cache()
	except Exception as e:
		print("Cache operation failed!")
		traceback.print_exc()
		print(e)

if __name__ == "__main__":
	main()
