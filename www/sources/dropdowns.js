 /* Hide Menu if user clicks outside */
window.onclick = function(e) {
	function hide() {
		Array.prototype.forEach.call(document.querySelectorAll("button.dropdown.dropmenushown, div.buttonbackgrounddropdown.dropmenushown"), function(el) {
				el.classList.remove("dropmenushown");
				el.nextElementSibling.style.maxHeight = null;
			}
		)
	}
	
	if (e.target.matches("button.dropdown") || e.target.matches("div.buttonbackgrounddropdown")) {
		if (e.target.matches(".dropmenushown")) {
			hide();
		} else {
			hide();
			var t = e.target;
			t.classList.add("dropmenushown");
			var c = t.nextElementSibling;
			c.style.maxHeight = c.scrollHeight + "px";
		}
	} else {
		hide();
	}
}

function dropdownSet(e, to) {
	var t = e.target;
	while (t.parentElement != null && !t.matches('.dropdown')) {
		t = t.parentElement;
	}
	if (t.matches('.dropdown')) {
		t.previousElementSibling.innerHTML = to;
	}
}
