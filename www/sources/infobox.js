function hideInfobox() {
	if(location.hash != "") {
		location.hash = "";
	}
	[].forEach.call(document.querySelectorAll("div.infobox.infoboxshown"), function(el) {el.classList.remove("infoboxshown")})
}

function showInfobox(id) {
	if(document.getElementById(id).matches("div.infobox#infoboxbg > div.infobox")) {
		document.getElementById("infoboxbg").classList.add("infoboxshown");
		document.getElementById(id).classList.add("infoboxshown");
	} else {throw TypeError()}
}

document.getElementById("infoboxbg").onclick = function(e) {
	if(e.target.matches("div.infobox.infoboxshown#infoboxbg")){
		hideInfobox();
	}
};

[].forEach.call(document.querySelectorAll("div#infoboxbg.infobox>div.infobox>h2.infoboxheader>button.infoboxclose"), function(e) {e.onclick = function(_e) {hideInfobox()}});

if(document.querySelector("div#infoboxbg.infobox>div.infobox:target") !== null) {showInfobox(document.querySelector("div#infoboxbg.infobox>div.infobox:target").getAttribute("id"))};


// Richtige Infobox beim Start zeigen
afterload.push(function(){if(document.querySelector("div#infoboxbg.infobox>div.infobox:target")!==null){showInfobox(document.querySelector("div#infoboxbg.infobox>div.infobox:target").getAttribute("id"))}})
