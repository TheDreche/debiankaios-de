var chartdata = null
var final_chart = null



function getData() {
	var req = new XMLHttpRequest();
	req.open('GET', 'sources/profilestats.json');
	req.responseType = 'json';
	req.send();
	req.onload = function() {
		chartdata = req.response.user_stats_history;
		drawChart();
	}
}

function drawChart() {
	const conf = {
		type: "line",
		data: chartdata,
		options: {
			responsive: true,
			scales: {
				y: {
					min: 0
				}
			},
			plugins: {
				title: {
					display: true,
					text: "Statistiken über die Zeit"
				}
			}
		}
	};
	const chart_element = document.querySelector("canvas.chartjs.profilestatschart1");
	final_chart = new Chart(chart_element, conf);
}

afterload.push(getData)
