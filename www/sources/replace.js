/* This file should replace data by newer data */

function replaceAll() {
	var req = new XMLHttpRequest();
	req.open('GET', 'sources/profilestats.json');
	req.responseType = 'json';
	req.send();
	req.onload = function() {
		var data = req.response;

		function replace(cls, by) {
			Array.prototype.forEach.call(document.getElementsByClassName(cls), function(i) {
				i.innerHTML = by;
			})
		}

		/* Data about the user debiankaios */
		var followercount = data.user_stats.follower.count.toLocaleString();
		var followerworld = data.user_stats.follower.rank_world.toLocaleString();
		var followercountry = data.user_stats.follower.rank_local.toLocaleString();
		
		// maxlens: [counts, worldranks, countryranks]
		var maxlens = [followercount.length, followerworld.length, followercountry.length];
		//< Saves the maximum lengths of the data as locale string

		function checkmax(counts, worldranks, countryranks) {
			if(counts > maxlens[0]) {
				maxlens[0] = counts;
			};
			if(worldranks > maxlens[1]) {
				maxlens[1] = worldranks;
			};
			if(countryranks > maxlens[2]) {
				maxlens[2] = countryranks;
			};
		}
		
		// Get the data and maximum string lengths (continued; above the function is the first part)
		var viewcount = data.user_stats.views.count.toLocaleString();
		var viewworld = data.user_stats.views.rank_world.toLocaleString();
		var viewcountry = data.user_stats.views.rank_local.toLocaleString();
		checkmax(viewcount.length, viewworld.length, viewcountry.length);
		
		var likecount = data.user_stats.likes.count.toLocaleString();
		var likeworld = data.user_stats.likes.rank_world.toLocaleString();
		var likecountry = data.user_stats.likes.rank_local.toLocaleString();
		checkmax(likecount.length, likeworld.length, likecountry.length);
		
		var favecount = data.user_stats.faves.count.toLocaleString();
		var faveworld = data.user_stats.faves.rank_world.toLocaleString();
		var favecountry = data.user_stats.faves.rank_local.toLocaleString();
		checkmax(favecount.length, faveworld.length, favecountry.length);
		
		var commentcount = data.user_stats.comments.count.toLocaleString();
		var commentworld = data.user_stats.comments.rank_world.toLocaleString();
		var commentcountry = data.user_stats.comments.rank_local.toLocaleString();
		checkmax(commentcount.length, commentworld.length, commentcountry.length);
		
		/**
		 * Fills the string str with the fillchar to length len and prefixes it with prefix
		 */
		function setstrlen(str, len, fillchar='\u00a0', prefix='') {
			other = '';
			for(var i = 0; i < len - str.length; i++) {
				other += fillchar;
			};
			return prefix + other + str;
		}
		
		// Set the length of the data (for alignment)
		followercount = setstrlen(followercount, maxlens[0], '\u00a0');
		followerworld = setstrlen(followerworld, maxlens[1], '\u00a0', '#');
		followercountry = setstrlen(followercountry, maxlens[2], '\u00a0', '#');
		
		viewcount = setstrlen(viewcount, maxlens[0], '\u00a0');
		viewworld = setstrlen(viewworld, maxlens[1], '\u00a0', '#');
		viewcountry = setstrlen(viewcountry, maxlens[2], '\u00a0', '#');
		
		likecount = setstrlen(likecount, maxlens[0], '\u00a0');
		likeworld = setstrlen(likeworld, maxlens[1], '\u00a0', '#');
		likecountry = setstrlen(likecountry, maxlens[2], '\u00a0', '#');
		
		favecount = setstrlen(favecount, maxlens[0], '\u00a0');
		faveworld = setstrlen(faveworld, maxlens[1], '\u00a0', '#');
		favecountry = setstrlen(favecountry, maxlens[2], '\u00a0', '#');
		
		commentcount = setstrlen(commentcount, maxlens[0], '\u00a0');
		commentworld = setstrlen(commentworld, maxlens[1], '\u00a0', '#');
		commentcountry = setstrlen(commentcountry, maxlens[2], '\u00a0', '#');
		
		replace('replacefollowercount', followercount);
		replace('replacefollowerworld', followerworld);
		replace('replacefollowercountry', followercountry);
		
		replace('replaceviewcount', viewcount);
		replace('replaceviewworld', viewworld);
		replace('replaceviewcountry', viewcountry);
		
		replace('replacelikecount', likecount);
		replace('replacelikeworld', likeworld);
		replace('replacelikecountry', likecountry);
		
		replace('replacefavecount', favecount);
		replace('replacefaveworld', faveworld);
		replace('replacefavecountry', favecountry);
		
		replace('replacecommentcount', commentcount);
		replace('replacecommentworld', commentworld);
		replace('replacecommentcountry', commentcountry);
		
		/* Data about his projects */
		replacenodes = document.getElementsByClassName('replaceprojectdata');
		var maxlen = 0; ///< The maximum length of the views column
		function insert(node) {
			replacenodes[0].parentNode.insertBefore(node, replacenodes[0]);
		}
		function addData(place, url, name, view, shared, explored) {
			datarootnode = document.createElement('tr');
			// placenode
			placenode = document.createElement('td');
			placenode.align = 'right';
			placenode.innerHTML = place.toLocaleString() + '.';
			datarootnode.appendChild(placenode);
			// namenode
			namenode = document.createElement('td');
			urlnode = document.createElement('a');
			urlnode.href = url;
			urlnode.target = '_blank';
			urlnode.rel = 'noopener noreferrer';
			urlnode.innerHTML = name;
			namenode.appendChild(urlnode);
			datarootnode.appendChild(namenode);
			// viewnode
			viewnode = document.createElement('td');
			viewnode.innerHTML = view;
			datarootnode.appendChild(viewnode);
			// sharednode
			sharednode = document.createElement('td');
			sharednode.innerHTML = shared;
			datarootnode.appendChild(sharednode);
			// explorednode
			explorednode = document.createElement('td');
			if(explored) {
				explorednode.innerHTML = '\u2714';
			} else {
				explorednode.innerHTML = '\u2718'
			}
			datarootnode.appendChild(explorednode);
			// Append everything
			insert(datarootnode);
		}
		// Find maximum length
		Array.prototype.forEach.call(data.projects_top, function(i) {
			if(i.views.toLocaleString().length > maxlen) {
				maxlen = i.views.toLocaleString().length;
			}
		});
		// Also care about total (sum) and average
		sum = data.project_stats.total;
		avg = data.project_stats.average;
		if(sum.views.toLocaleString().length > maxlen) {
			maxlen = sum.views.toLocaleString().length;
		}
		if(avg.views.toLocaleString().length > maxlen) {
			maxlen = avg.views.toLocaleString().length;
		}
		// Add data
		for(var i = 0; i < data.projects_top.length; i++) {
			addData((i + 1).toLocaleString(), data.projects_top[i].url, data.projects_top[i].name, setstrlen(data.projects_top[i].views.toLocaleString(), maxlen, '\u00a0'), data.projects_top[i].share_date, data.projects_top[i].explore);
		}
		// Remove old data
		while(replacenodes.length > 0) {
			replacenodes[0].remove();
		}
		// Replace the single-value-data
		replace('replacesumview', setstrlen(data.project_stats.total.views.toLocaleString(), maxlen, '\u00a0'));
		replace('replacesumdate', data.date);
		replace('replaceavgview', setstrlen(data.project_stats.average.views.toLocaleString(), maxlen, '\u00a0'));
		replace('replaceavgdate', data.date);
	}
}

afterload.push(replaceAll);
