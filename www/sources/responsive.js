function resizeIFrameToFitContent( iFrame ) {
    iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
}

window.addEventListener('DOMContentLoaded', function(e) {
    Array.prototype.forEach.call(document.querySelectorAll("iframe.responsive1"), resizeIFrameToFitContent)
}
					   );
