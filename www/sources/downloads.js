const downloads = {
	"Pixel Eating": {
		"latest": "1.0.4",
		"versions": [
			{
				"name": "1.0.4",
				"types": [
					{"download": true, "suffix": ".zip", "path": "/Downloads/Pixel_Eating-1.0.4.zip"},
					{"download": true, "suffix": ".tar.gz", "path": "/Downloads/Pixel_Eating-1.0.4.tar.gz"},
				],
			},
			{
				"name": "1.0.2",
				"types": [
					{"download": true, "suffix": ".zip", "path": "/Downloads/Pixel_Eating-1.0.2.zip"},
					{"download": true, "suffix": ".tar.gz", "path": "/Downloads/Pixel_Eating-1.0.2.tar.gz"},
				],
			},
			{
				"name": "1.0.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "/Downloads/Pixel_Eating-1.0.1.zip"},
					{"download": true, "suffix": ".tar.gz", "path": "/Downloads/Pixel_Eating-1.0.1.tar.gz"},
				],
			},
			{
				"name": "1.0",
				"types": [
					{"download": true, "suffix": ".zip", "path": "/Downloads/Pixel_Eating.zip"},
					{"download": true, "suffix": ".tar.gz", "path": "/Downloads/Pixel_Eating.tar.gz"},
				],
			},
		],
	},
	"New Lights": {
		"latest": [
			{"dowload": true, "suffix": ".zip", "path": "/Downloads/NewLights.zip"},
		],
	},
	"TA_ESPE": {
		"latest": [
			{"download": true, "suffix": ".zip", "path": "https://codeberg.org/debiankaios/ta_espe/archive/master.zip"},
		],
	},
	"Alien Material": {
		"latest": "0.10",
		"versions": [
			{
				"name": "0.10",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/alien_material/releases/6031/download/"},
				],
			},
			{
				"name": "BETA 20.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/alien_material/releases/5747/download/"},
				],
			},
			{
				"name": "0.9",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/alien_material/releases/5657/download/"},
				],
			},
			{
				"name": "0.8",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/alien_material/releases/5654/download/"},
				],
			},
		],
	},
	"Proxima Survival": {
		"latest": "Alpha 0.1.1",
		"versions": [
			{
				"name": "Alpha 0.1.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/16508/download/"},
				],
			},
			{
				"name": "Alpha 0.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/10624/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.7.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/10242/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.7",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/9964/download/"},
				],
			},
			{
				"name": "November Update",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/9809/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.5.2",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/9308/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.5.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/9143/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.5",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/9045/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.4.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/8942/download/"},
				],
			},
			{
				"name": "Posometer-Update",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/8789/download/"},
				],
			},
			{
				"name": "Short Mapgen Update",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/8780/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.2.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/8779/download/"},
				],
			},
			{
				"name": "Pre Alpha 0.2",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/proxima_survival/releases/8767/download/"},
				],
			},
		],
	},
	"SimpleTextures": {
		"latest": "0.6",
		"versions": [
			{
				"name": "0.6",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/simpletextures/releases/6440/download/"},
				],
			},
			{
				"name": "0.5",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/simpletextures/releases/5914/download/"},
				],
			},
			{
				"name": "0.4.3",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/simpletextures/releases/5913/download/"},
				],
			},
			{
				"name": "0.4.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/simpletextures/releases/5834/download/"},
				],
			},
			{
				"name": "0.4",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/simpletextures/releases/5833/download/"},
				],
			},
		],
	},
	"TNTRun": {
		"latest": "1.0",
		"versions": [
			{
				"name": "1.0",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/19042/download/"},
				],
			},
			{
				"name": "0.9",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/7729/download/"},
				],
			},
			{
				"name": "2021-05-03",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/7672/download/"},
				],
			},
			{
				"name": "0.8",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6310/download/"},
				],
			},
			{
				"name": "0.7.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6284/download/"},
				],
			},
			{
				"name": "0.7",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6283/download/"},
				],
			},
			{
				"name": "0.6",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6263/download/"},
				],
			},
			{
				"name": "0.5.5",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6242/download/"},
				],
			},
			{
				"name": "0.5.4",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6241/download/"},
				],
			},
			{
				"name": "0.5.3",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6240/download/"},
				],
			},
			{
				"name": "0.5.2",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6239/download/"},
				],
			},
			{
				"name": "0.5.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6236/download/"},
				],
			},
			{
				"name": "0.5",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6228/download/"},
				],
			},
			{
				"name": "0.4",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tntrun/releases/6201/download/"},
				],
			},
		],
	},
	"TNTTag": {
		"latest": "0.2",
		"versions": [
			{
				"name": "0.2",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tnttag/releases/14553/download/"},
				],
			},
			{
				"name": "0.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/tnttag/releases/14233/download/"},
				],
			},
		],
	},
	"XP Highscores": {
		"latest": "Testing 0.1.1",
		"versions": [
			{
				"name": "Testing 0.1.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/xp_highscores/releases/13804/download/"},
				],
			},
			{
				"name": "Testing 0.1",
				"types": [
					{"download": true, "suffix": ".zip", "path": "https://content.minetest.net/packages/debiankaios/xp_highscores/releases/11888/download/"},
				],
			},
		],
	},
};

function downloadRefresh(t, project, exclude_version, versiono, filetype) {
	/* Richtiger dowload */
	filetype = versiono["types"].find(function(o) {return o["suffix"] == filetype});
	t.querySelector('div.confirm > a').href = filetype.path;
	if(filetype.download == true) {
		if(exclude_version) {
			t.querySelector('div.confirm > a').download = project.replaceAll(' ', '_')+filetype;
		} else {
			t.querySelector('div.confirm > a').download = project.replaceAll(' ', '_')+'-'+versiono.name+filetype;
		}
	} else if(typeof filetype.download == "string") {
		t.querySelector("div.confirm > a").download = filetype.download;
	} else {
		t.querySelector("div.confirm > a").removeAttribute("download");
	}
}

function refreshFTs(t, versiono) {
	const ftbutton = t.querySelector("div.filetype > button");
	const ftul = t.querySelector("div.filetype > div.dropdown > ul");
	ftul.textContent = "";
	let hasval = true;
	const ftsuffix = t.querySelector('div.filetype > button').textContent;
	if(!versiono.some(function(o) {return o.suffix == ftsuffix})) {
		console.log("NOT THERE!");
		hasval = false;
	}
	for(const ftd of versiono) {
		const ft = ftd["suffix"];
		if(!hasval) {
			ftbutton.innerText = ft;
		}
		const e = document.createElement("li");
		e.onclick = downloadDropdownSet;

		const b = document.createElement("button");
		b.type = "button";
		b.classList.add("round");
		b.classList.add("end");
		b.classList.add("download");
		b.classList.add("nohover");
		b.textContent = ft;
		e.appendChild(b);

		ftul.appendChild(e);
	}
}

function downloadRefreshAuto(t) {
	const name = t.querySelector('div.confirm > a > button').innerHTML;
	let version = {};
	if(t.querySelectorAll('div.version').length>0) {
		const tversion = t.querySelector('div.version > button').innerHTML;
		version = downloads[name]["versions"].find(function(v) {return v["name"] == tversion});
		refreshFTs(t, version.types);
		const filetype = t.querySelector('div.filetype > button').innerHTML;
		downloadRefresh(t, name, false, version, filetype);
	} else {
		version = downloads[name]["latest"];
		if(typeof version == "string") {
			const tversion = version;
			version = downloads[name]["versions"].find(function(v) {return v.name = tversion});
		}
		refreshFTs(t, version);
		const filetype = t.querySelector('div.filetype > button').textContent;
		downloadRefresh(t, name, true, {"types": version}, filetype);
	}
}

function downloadRefreshOutof(t) {
	while(!(t.matches('div.download') || t.parentElement == null)) {
		t = t.parentNode;
	}
	if(t.matches('div.download')) {
		downloadRefreshAuto(t);
	}
}

function downloadDropdownSet(e) {
	dropdownSet(e, e.target.innerText);
	downloadRefreshOutof(e.target);
}

afterload.push(function() {
	Array.prototype.forEach.call(document.querySelectorAll('div.download'), downloadRefreshAuto);
});
