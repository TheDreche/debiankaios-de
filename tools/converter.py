#!/bin/python3

# This script helps converting the previous data format
# (JS-style, similar to JSON) into the new data format
# (strict JSON, but different names).
# 
# The strictness can be skipped because it is just
# replacing ' by " which can be easily done manually
# (usintg search and replace)

import json

def main():
	in_ = input('Data: ')
	c1: list = json.loads(in_)
	out: list = []
	for i in c1:
		out.append({"date": i[0], "view": i[1], "comment": i[2], "like": i[3], "fave": i[4], "follower": i[5]})
	print(json.dumps(out, indent='\t'))

if __name__ == "__main__":
	main()
